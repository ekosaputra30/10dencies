<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <link rel="icon" href="img/fav-icon.png" type="image/x-icon" />
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>@yield('page_title')</title>

        <!-- Icon css link -->
        <link href="{{asset('themes/persuit/assets/css/font-awesome.min.css')}}" rel="stylesheet">
        <link href="{{asset('themes/persuit/assets/vendors/line-icon/css/simple-line-icons.css')}}" rel="stylesheet">
        <link href="{{asset('themes/persuit/assets/vendors/elegant-icon/style.css')}}" rel="stylesheet">
        <!-- Bootstrap -->
        <link href="{{asset('themes/persuit/assets/css/bootstrap.min.css')}}" rel="stylesheet">
        
        <!-- Rev slider css -->
        <link href="{{asset('themes/persuit/assets/vendors/revolution/css/settings.css')}}" rel="stylesheet">
        <link href="{{asset('themes/persuit/assets/vendors/revolution/css/layers.css')}}" rel="stylesheet">
        <link href="{{asset('themes/persuit/assets/vendors/revolution/css/navigation.css')}}" rel="stylesheet">
        
        <!-- Extra plugin css -->
        <link href="{{asset('themes/persuit/assets/vendors/owl-carousel/owl.carousel.min.css')}}" rel="stylesheet">
        <link href="{{asset('themes/persuit/assets/vendors/bootstrap-selector/css/bootstrap-select.min.css')}}" rel="stylesheet">
        
        <link href="{{ asset('themes/persuit/assets/css/style.css') }}" rel="stylesheet">
        <link href="{{ asset('themes/persuit/assets/css/responsive.css') }}" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <header>
            <!--================Menu Area =================-->
            <div class="shop_header_area">
                <div class="container-fluid">
                    <nav class="navbar navbar-expand-lg navbar-light bg-light">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <a class="navbar-brand ml-2 mr-auto" href="#"><img src="img/logo-black.png" class="img-fluid" alt=""></a>
                        <ul class="navbar-nav top_right d-flex flex-row flex-wrap d-lg-none mt-0">
                            <li class="user mr-1"><a href="login.html"><i class="icon-user icons"></i></a></li>
                            <li class="cart mr-1"><a href="cart.html"><i class="icon-handbag icons"></i></a></li>
                        </ul>

                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav align-items-start align-items-lg-end">
                                <li class="nav-item ml-2 mr-3 d-none d-lg-flex">
                                    <a href="./index.html" class="brand-logo">
                                        <img src="img/logo-black.png" alt="" class="img-fluid mt-2">
                                    </a>
                                </li>
                                <li class="nav-item dropdown submenu active">
                                    <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Shop <i class="fa fa-angle-down" aria-hidden="true"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-large flex-wrap flex-column">
                                        <ul>
                                            <li class="nav-item nav-category">Tees</li>
                                            <li class="nav-item"><a class="nav-link" href="shop.html">Heavy Cotton Tees</a></li>
                                        </ul>
                                        <ul>
                                            <li class="nav-item nav-category">Graphic Tees</li>
                                            <li class="nav-item"><a class="nav-link" href="shop.html">Short Sleeve</a></li>
                                            <li class="nav-item"><a class="nav-link" href="shop.html">Long Sleeve</a></li>
                                        </ul>
                                        <ul>
                                            <li class="nav-item nav-category">Basic Tees</li>
                                            <li class="nav-item"><a class="nav-link" href="shop.html">Short Sleeve</a></li>
                                            <li class="nav-item"><a class="nav-link" href="shop.html">Long Sleeve</a></li>
                                        </ul>
                                        <ul>
                                            <li class="nav-item nav-category">Tops</li>
                                            <li class="nav-item"><a class="nav-link" href="shop.html">Shirt</a></li>
                                            <li class="nav-item"><a class="nav-link" href="shop.html">Jackets</a></li>
                                            <li class="nav-item"><a class="nav-link" href="shop.html">Hoodies & Sweaters</a></li>
                                        </ul>
                                        <ul>
                                            <li class="nav-item nav-category">Bottoms</li>
                                            <li class="nav-item"><a class="nav-link" href="shop.html">Pants</a></li>
                                            <li class="nav-item"><a class="nav-link" href="shop.html">Chinos</a></li>
                                            <li class="nav-item"><a class="nav-link" href="shop.html">Shorts</a></li>
                                            <li class="nav-item"><a class="nav-link" href="shop.html">Jeans</a></li>
                                        </ul>
                                        <ul>
                                            <li class="nav-item nav-category">Accessories</li>
                                            <li class="nav-item"><a class="nav-link" href="shop.html">Bags</a></li>
                                            <li class="nav-item"><a class="nav-link" href="shop.html">Hats</a></li>
                                            <li class="nav-item"><a class="nav-link" href="shop.html">Belts</a></li>
                                            <li class="nav-item"><a class="nav-link" href="shop.html">Wallets</a></li>
                                            <li class="nav-item"><a class="nav-link" href="shop.html">Others</a></li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="nav-item"><a class="nav-link" href="#">News</a></li>
                                <li class="nav-item"><a class="nav-link" href="#">Promo</a></li>
                                <li class="nav-item"><a class="nav-link" href="#">Discover</a></li>
                                <li class="nav-item"><a class="nav-link" href="contact.html">Contact</a></li>
                            </ul>
                            <ul class="ml-auto mt-0 top_right">
                                <li class="mr-1">
                                    <div class="top_header_left pt-0">
                                        <div class="input-group mt-0">
                                            <input type="text" class="form-control" placeholder="Search" aria-label="Search">
                                            <span class="input-group-btn">
                                            <button class="btn btn-secondary" type="button"><i class="icon-magnifier"></i></button>
                                            </span>
                                        </div>
                                    </div>
                                </li>
                                <li class="user mr-1"><a href="login.html"><i class="icon-user icons"></i></a></li>
                                <li class="cart mr-1"><a href="cart.html"><i class="icon-handbag icons"></i></a></li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
            <!--================End Menu Area =================-->
        </header>
        
        <main>
            <!--================Slider Area =================-->
            <section class="main_slider_area">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div id="main_slider" class="rev_slider" data-version="5.3.1.6">
                                <ul>
                                    <li data-index="rs-1587" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-thumb="img/home-slider/slider-1.jpg"  data-rotate="0"  data-saveperformance="off"  data-title="Creative">
                                        <!-- MAIN IMAGE -->
                                        <img src="https://placehold.it/1920x800"  alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                                    </li>
                                    <li data-index="rs-1588" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-thumb="img/home-slider/slider-2.jpg"  data-rotate="0"  data-saveperformance="off"  data-title="Creative">
                                        <!-- MAIN IMAGE -->
                                        <img src="https://placehold.it/1920x800"  alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--================End Slider Area =================-->
            
            <!--================Home Menu Area =================-->
            <div class="home_menu_area mt-4">
                <div class="row d-sm-none no-gutters">
                    <div class="col-6">
                        <div class="card">
                            <div class="card-body text-center">
                                <h6 class="card-title mb-0">New Arrivals</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="card">
                            <div class="card-body text-center">
                                <h6 class="card-title mb-0">Tees</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="card">
                            <div class="card-body text-center">
                                <h6 class="card-title mb-0">Graphic Tees</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="card">
                            <div class="card-body text-center">
                                <h6 class="card-title mb-0">Basic Tees</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="card">
                            <div class="card-body text-center">
                                <h6 class="card-title mb-0">Tops</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="card">
                            <div class="card-body text-center">
                                <h6 class="card-title mb-0">Bottoms</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="card">
                            <div class="card-body text-center">
                                <h6 class="card-title mb-0">Accessories</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--================End Home Menu Area =================-->
            <!--================Feature Big Add Area =================-->
            <section class="feature_big_add_area pt-4 pb-0">
                <div class="container-fluid p-sm-0">
                    <div class="row no-gutters">
                        <div class="col-lg-4">
                            <div class="f_add_item">
                                <a class="add_btn" href="shop.html">
                                    <div class="f_add_img"><img class="img-fluid" src="https://placehold.it/573x575" alt=""></div>
                                    <div class="f_add_hover">
                                        <h4>Best Collection</h4>
                                        Shop Now <i class="arrow_right"></i>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="f_add_item">
                                <a class="add_btn" href="shop.html">
                                    <div class="f_add_img"><img class="img-fluid" src="https://placehold.it/573x575" alt=""></div>
                                    <div class="f_add_hover">
                                        <h4>Best Collection</h4>
                                        Shop Now <i class="arrow_right"></i>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="f_add_item">
                                <a class="add_btn" href="shop.html">
                                    <div class="f_add_img"><img class="img-fluid" src="https://placehold.it/573x575" alt=""></div>
                                    <div class="f_add_hover">
                                        <h4>Best Collection</h4>
                                        Shop Now <i class="arrow_right"></i>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--================End Feature Big Add Area =================-->
            
            <!--================Feature Big Add Area =================-->
            <section class="feature_big_add_area second pt-4">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="f_add_item absolute white_add">
                                <a class="add_btn" href="shop.html">
                                    <div class="f_add_img"><img class="img-fluid" src="https://placehold.it/862x575" alt=""></div>
                                    <div class="f_add_hover">
                                        <h4>Best Collection</h4>
                                        Shop Now <i class="arrow_right"></i>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="f_add_item absolute white_add">
                                <a class="add_btn" href="shop.html">
                                    <div class="f_add_img"><img class="img-fluid" src="https://placehold.it/862x575" alt=""></div>
                                    <div class="f_add_hover">
                                        <h4>Best Collection</h4>
                                        Shop Now <i class="arrow_right"></i>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--================End Feature Big Add Area =================-->
            <!--================Our Latest Product Area =================-->
            <section class="our_latest_product">
                <div class="container-fluid">
                    <div class="s_m_title">
                        <h2>Featured Products</h2>
                    </div>
                    <div class="l_product_slider owl-carousel">
                        <div class="item">
                            <div class="l_product_item">
                                <div class="l_p_img">
                                    <a href="shop-detail.html" class="d-flex">
                                        <img src="https://placehold.it/270x320" alt="">
                                    </a>
                                </div>
                                <div class="l_p_text">
                                    <h4>Womens Libero</h4>
                                    <h5><del>$45.50</del>  $40</h5>
                                </div>
                                <div class="l_p_add">
                                    <a class="add_cart_btn" href="#"><i class="icon_cart_alt"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="l_product_item">
                                <div class="l_p_img">
                                    <a href="shop-detail.html" class="d-flex">
                                        <img src="https://placehold.it/270x320" alt="">
                                    </a>
                                </div>
                                <div class="l_p_text">
                                    <h4>Travel Bags</h4>
                                    <h5><del>$45.50</del>  $40</h5>
                                </div>
                                <div class="l_p_add">
                                    <a class="add_cart_btn" href="#"><i class="icon_cart_alt"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="l_product_item">
                                <div class="l_p_img">
                                    <a href="shop-detail.html" class="d-flex">
                                        <img src="https://placehold.it/270x320" alt="">
                                    </a>
                                </div>
                                <div class="l_p_text">
                                    <h4>Summer Dress</h4>
                                    <h5>$45.05</h5>
                                </div>
                                <div class="l_p_add">
                                    <a class="add_cart_btn" href="#"><i class="icon_cart_alt"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="l_product_item">
                                <div class="l_p_img">
                                    <a href="shop-detail.html" class="d-flex">
                                        <img src="https://placehold.it/270x320" alt="">
                                    </a>
                                </div>
                                <div class="l_p_text">
                                    <h4>Nike Shoes</h4>
                                    <h5><del>$130</del> $110</h5>
                                </div>
                                <div class="l_p_add">
                                    <a class="add_cart_btn" href="#"><i class="icon_cart_alt"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--================End Our Latest Product Area =================-->
            <!--================New Arrivals Area =================-->
            <section class="our_latest_product">
                <div class="container-fluid">
                    <div class="s_m_title">
                        <h2>New Arrivals</h2>
                    </div>
                    <div class="l_product_slider owl-carousel">
                        <div class="item">
                            <div class="l_product_item">
                                <div class="l_p_img">
                                    <a href="shop-detail.html" class="d-flex">
                                        <img src="https://placehold.it/270x320" alt="">
                                    </a>
                                </div>
                                <div class="l_p_text">
                                    <h4>Womens Libero</h4>
                                    <h5><del>$45.50</del>  $40</h5>
                                </div>
                                <div class="l_p_add">
                                    <a class="add_cart_btn" href="#"><i class="icon_cart_alt"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="l_product_item">
                                <div class="l_p_img">
                                    <a href="shop-detail.html" class="d-flex">
                                        <img src="https://placehold.it/270x320" alt="">
                                    </a>
                                </div>
                                <div class="l_p_text">
                                    <h4>Travel Bags</h4>
                                    <h5><del>$45.50</del>  $40</h5>
                                </div>
                                <div class="l_p_add">
                                    <a class="add_cart_btn" href="#"><i class="icon_cart_alt"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="l_product_item">
                                <div class="l_p_img">
                                    <a href="shop-detail.html" class="d-flex">
                                        <img src="https://placehold.it/270x320" alt="">
                                    </a>
                                </div>
                                <div class="l_p_text">
                                    <h4>Summer Dress</h4>
                                    <h5>$45.05</h5>
                                </div>
                                <div class="l_p_add">
                                    <a class="add_cart_btn" href="#"><i class="icon_cart_alt"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="l_product_item">
                                <div class="l_p_img">
                                    <a href="shop-detail.html" class="d-flex">
                                        <img src="https://placehold.it/270x320" alt="">
                                    </a>
                                </div>
                                <div class="l_p_text">
                                    <h4>Nike Shoes</h4>
                                    <h5><del>$130</del> $110</h5>
                                </div>
                                <div class="l_p_add">
                                    <a class="add_cart_btn" href="#"><i class="icon_cart_alt"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--================End New Arrivals Area =================-->
            
            <!--================Form Blog Area =================-->
            <section class="from_blog_area">
                <div class="container-fluid from_blog_inner">
                    <div class="s_m_title">
                        <h2>From The Blog</h2>
                    </div>

                    <div class="row no-gutters">
                        <div class="col-6 col-lg-3 col-sm-6">
                            <div class="from_blog_item">
                                <a href="">
                                    <img class="img-fluid" src="https://placehold.it/410x255" alt="">
                                </a>
                                <div class="f_blog_text">
                                    <h5>fashion</h5>
                                    <a href="">Neque porro quisquam est qui dolorem ipsum</a>
                                    <h6 class="d-none">21.09.2017</h6>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 col-lg-3 col-sm-6">
                            <div class="from_blog_item">
                                <a href="">
                                    <img class="img-fluid" src="https://placehold.it/410x255" alt="">
                                </a>
                                <div class="f_blog_text">
                                    <h5>fashion</h5>
                                    <a href="">Neque porro quisquam est qui dolorem ipsum</a>
                                    <h6 class="d-none">21.09.2017</h6>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 col-lg-3 col-sm-6">
                            <div class="from_blog_item">
                                <a href="">
                                    <img class="img-fluid" src="https://placehold.it/410x255" alt="">
                                </a>
                                <div class="f_blog_text">
                                    <h5>fashion</h5>
                                    <a href="">Neque porro quisquam est qui dolorem ipsum</a>
                                    <h6 class="d-none">21.09.2017</h6>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 col-lg-3 col-sm-6">
                            <div class="from_blog_item">
                                <a href="">
                                    <img class="img-fluid" src="https://placehold.it/410x255" alt="">
                                </a>
                                <div class="f_blog_text">
                                    <h5>fashion</h5>
                                    <a href="">Neque porro quisquam est qui dolorem ipsum</a>
                                    <h6 class="d-none">21.09.2017</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--================End Form Blog Area =================-->
        </main>

        <!--================Footer Area =================-->
        <footer class="footer_area">
            <div class="container-fluid">
                <div class="footer_widgets">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-6">
                            <aside class="d-flex f_about_widget f_widget flex-column h-100 justify-content-center">
                                <a href="" class="footer-logo">
                                    <img src="img/logo-black.png" alt="" class="img-fluid">
                                </a>
                                <div>
                                    <h6>Social:</h6>
                                    <ul>
                                        <li><a href="#"><i class="social_facebook"></i></a></li>
                                        <li><a href="#"><i class="social_twitter"></i></a></li>
                                        <li><a href="#"><i class="social_pinterest"></i></a></li>
                                        <li><a href="#"><i class="social_instagram"></i></a></li>
                                        <li><a href="#"><i class="social_youtube"></i></a></li>
                                    </ul>
                                </div>
                            </aside>
                        </div>
                        <div class="col-lg-2 col-md-4 col-6">
                            <aside class="f_widget link_widget f_info_widget">
                                <div class="f_w_title">
                                    <h3>Information</h3>
                                </div>
                                <ul>
                                    <li><a href="#">About us</a></li>
                                    <li><a href="#">Delivery information</a></li>
                                    <li><a href="#">Terms & Conditions</a></li>
                                    <li><a href="#">Help Center</a></li>
                                    <li><a href="#">Returns & Refunds</a></li>
                                </ul>
                            </aside>
                        </div>
                        <div class="col-lg-2 col-md-4 col-6">
                            <aside class="f_widget link_widget f_service_widget">
                                <div class="f_w_title">
                                    <h3>Customer Service</h3>
                                </div>
                                <ul>
                                    <li><a href="#">My account</a></li>
                                    <li><a href="#">Ordr History</a></li>
                                    <li><a href="#">Wish List</a></li>
                                    <li><a href="#">Newsletter</a></li>
                                    <li><a href="#">Contact Us</a></li>
                                </ul>
                            </aside>
                        </div>
                        <div class="col-lg-2 col-md-4 col-6">
                            <aside class="f_widget link_widget f_extra_widget">
                                <div class="f_w_title">
                                    <h3>Extras</h3>
                                </div>
                                <ul>
                                    <li><a href="#">Brands</a></li>
                                    <li><a href="#">Gift Vouchers</a></li>
                                    <li><a href="#">Affiliates</a></li>
                                    <li><a href="#">Specials</a></li>
                                </ul>
                            </aside>
                        </div>
                        <div class="col-lg-2 col-md-4 col-6">
                            <aside class="f_widget link_widget f_account_widget">
                                <div class="f_w_title">
                                    <h3>My Account</h3>
                                </div>
                                <ul>
                                    <li><a href="#">My account</a></li>
                                    <li><a href="#">Ordr History</a></li>
                                    <li><a href="#">Wish List</a></li>
                                    <li><a href="#">Newsletter</a></li>
                                </ul>
                            </aside>
                        </div>
                    </div>
                </div>
                <div class="footer_copyright">
                    <h5>&copy;<script>document.write(new Date().getFullYear());</script> All rights reserved
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
</h5>
                </div>
            </div>
        </footer>
        <!--================End Footer Area =================-->
        
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="{{asset('themes/persuit/assets/js/jquery-3.2.1.min.js')}}"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="{{asset('themes/persuit/assets/js/popper.min.js')}}"></script>
        <script src="{{asset('themes/persuit/assets/js/bootstrap.min.js')}}"></script>
        <!-- Rev slider js -->
        <script src="{{asset('themes/persuit/assets/vendors/revolution/js/jquery.themepunch.tools.min.js')}}"></script>
        <script src="{{asset('themes/persuit/assets/vendors/revolution/js/jquery.themepunch.revolution.min.js')}}"></script>
        <script src="{{asset('themes/persuit/assets/vendors/revolution/js/extensions/revolution.extension.actions.min.js')}}"></script>
        <script src="{{asset('themes/persuit/assets/vendors/revolution/js/extensions/revolution.extension.video.min.js')}}"></script>
        <script src="{{asset('themes/persuit/assets/vendors/revolution/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
        <script src="{{asset('themes/persuit/assets/vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
        <script src="{{asset('themes/persuit/assets/vendors/revolution/js/extensions/revolution.extension.navigation.min.js')}}"></script>
        <!-- Extra plugin css -->
        <script src="{{asset('themes/persuit/assets/vendors/counterup/jquery.waypoints.min.js')}}"></script>
        <script src="{{asset('themes/persuit/assets/vendors/counterup/jquery.counterup.min.js')}}"></script>
        <script src="{{asset('themes/persuit/assets/vendors/owl-carousel/owl.carousel.min.js')}}"></script>
        <script src="{{asset('themes/persuit/assets/vendors/bootstrap-selector/js/bootstrap-select.min.js')}}"></script>
        <script src="{{asset('themes/persuit/assets/vendors/image-dropdown/jquery.dd.min.js')}}"></script>
        <script src="{{asset('themes/persuit/assets/js/smoothscroll.js')}}"></script>
        <script src="{{asset('themes/persuit/assets/vendors/isotope/imagesloaded.pkgd.min.js')}}"></script>
        <script src="{{asset('themes/persuit/assets/vendors/isotope/isotope.pkgd.min.js')}}"></script>
        <script src="{{asset('themes/persuit/assets/vendors/magnify-popup/jquery.magnific-popup.min.js')}}"></script>
        <script src="{{asset('themes/persuit/assets/vendors/vertical-slider/js/jQuery.verticalCarousel.js')}}"></script>
        <script src="{{asset('themes/persuit/assets/vendors/jquery-ui/jquery-ui.js')}}"></script>
        
        <script src="{{asset('themes/persuit/assets/js/theme.js')}}"></script>
    </body>
</html>